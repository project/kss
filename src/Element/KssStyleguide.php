<?php

/**
 * @file
 * Contains \Drupal\toolbar\Element\Toolbar.
 */

namespace Drupal\kss\Element;

use Drupal\Core\Render\Element\RenderElement;

/**
 * Provides a render element for the kss styleguide.
 *
 * @RenderElement("kss_styleguide")
 */
class KssStyleguide extends RenderElement {

  /**
   * {@inheritdoc}
   */
  public function getInfo() {
    return array(
      '#theme' => 'kss_styleguide',
      '#attached' => array(
        'library' => array(
          'kss/kss.styleguide',
        ),
      ),
    );
  }

  /**
   * Wraps the module handler.
   *
   * @return \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected static function moduleHandler() {
    return \Drupal::moduleHandler();
  }

}
