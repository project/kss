<?php

namespace Drupal\kss\Controller;

class StyleguideController {

  public function show() {

    // Get css from active Theme
    $theme_info = \Drupal::theme()->getActiveTheme();

    // render kss php renderer
    return array(
      '#type' => 'kss_styleguide',
      '#kss_source' => $theme_info->getPath(),
    );

  }

}