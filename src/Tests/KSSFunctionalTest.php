<?php

/**
 * @file
 * Contains Drupal\kss\Tests\KSSFunctionalTest.
 */

namespace Drupal\kss\Tests;
use Drupal\simpletest\WebTestBase;

/**
 * Tests permissions and KSS processing.
 *
 * @group kss
 */
class KSSFunctionalTest extends WebTestBase {

  private $anon_user = NULL;
  private $web_user = NULL;


  /**
   * Modules to enable.
   *
   * @var array
   */
  public static $modules = array('kss');

  protected function setUp() {
    parent::setUp();

    // Enable KSS Test theme
    \Drupal::service('theme_handler')->install(array('kss_test_theme'));
    \Drupal::service('theme_handler')->setDefault('kss_test_theme');

    // Create User
    $this->anon_user = $this->drupalCreateUser(array());
    $this->web_user = $this->drupalCreateUser(array('access styleguide page'));

  }


  /**
   * Test permissions and if the KSS module processes the active theme.
   */
  function testKSSProcessing() {

    // User without the permission should not see the styleguide
    $this->drupalLogin($this->anon_user);
    $this->drupalGet('styleguide');
    $this->assertResponse(403);
    $this->drupalLogout();

    // User with the permission should see the styleguide
    $this->drupalLogin($this->web_user);
    $this->drupalGet('styleguide');
    $this->assertResponse(200);

    $this->assertEqual((string)$this->cssSelect('h1')[0], 'Styleguide');

    /**
     * @see Drupal\kss\kss_test_theme for all KSS test files.
     */

    // check if there was a menu entry created
    $this->assertEqual((string)$this->cssSelect('nav a[href="#kss-section-simple"]')[0], 'simple button');

    // @todo check for tree menu.

    // check if the css was processed right.
    $this->assertEqual((string)$this->cssSelect('article[id="kss-section-simple"] h2')[0], 'simple button');

    // @todo check more complex kss.
  }

}
